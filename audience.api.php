<?php



/**
 * Hook to react on circumstances that change a specific audience.
 *
 * E.g. if a node changed the userreference valus, alle audiences haveing the
 * userreference audience type will change according to this node.
 *
 * If a audience preset changes, so all according audiences will change,
 * hook_audience_preset_update() should be used, as we then do not process any
 * single audience of this type by hook_audience_update().
 *
 *
 * @param Audience $audience
 */
function hook_audience_update($audience) {

}

/**
 * This hook is invoked, when cictumstances change so an audience is not valid
 * or present anymore.
 *
 * E.g. if a node is deleted, all assigned audiences will disappear.
 *
 * @param Audience $audience
 */
function hook_audience_delete($audience) {

}


/**
 * This hook is invoked when a preset definition changes, or another change may
 * change any single audience related to this audience preset.
 *
 * @param object $audience_preset
 */
function hook_audience_preset_update($audience_preset) {

}

/**
 * @param object $audience_preset
 */
function hook_audience_preset_delete($audience_preset) {

}