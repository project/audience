<?php

/**
 * @file
 *   provides the 'role' audience type to audience.
 */

$plugin = array(
  'label' => t('Union of presets'),
  'description' => t('Build a preset by unite different presets.'),
  'context' => 'node',
  'members callback' => '_audience_type_members_union',
  'is member callback' => '_audience_type_is_member_of_union',
  'circumstances changed callback' => '_audience_type_union_circumstances_changed',
  'config title' => t('Presets'),
  'config description' => t('Select the presets to <em>OR</em>.'),
  'config options' => 'audience_presets_as_optionsarray',
  'config multiple' => TRUE,
);

/**
 * Callback function to specify if user is in any preset defined be the audience
 * bundle.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_union($audience_preset, $context, $account) {
  $presets = $audience_preset->config;

  // Are any presets selected?
  if (is_array($presets) && count($presets)) {
    foreach ($presets as $preset_name) {
      $return = audience_is_member_of($preset_name, $context, $account);
      // Return true on success as the presets are ORed
      if ($return) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Callback function to retrieve the merge of users in the bundled presets.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_union($audience_preset, $context = array()) {
  $presets = $audience_preset->config;

  // Are any presets selected?
  if (is_array($presets) && count($presets)) {
    $users = array();
    foreach ($presets as $preset_name) {
      $add_users = audience_members($preset_name, $context);
      $users = array_merge($users, $add_users);
    }
    $users = array_unique($users);
    return $users;
  }
  return array();
}


/**
 * Callback function to react on changed circumstances for the union audience type.
 *
 * @param string $mode
 * @param mixed $data
 */
function _audience_type_union_circumstances_changed($mode, $data) {
  // An audience preset has updated, so we need to update presets, that contain
  // that preset.
  if (drupal_substr($mode, 0, 16) == 'audience_preset:') {
    $data_preset_name = $data->name;
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'union' && in_array($data_preset_name, $preset->config)) {
        audience_preset_update($preset);
      }
    }
  }
  // An audience is affected, so we have to update audiences, that contain the
  // given audience as intersection element.
  elseif (drupal_substr($mode, 0, 8) == 'audience') {
    $data_preset_name = $data->getPresetName();
    // Check if the audience is global. Checking on context(=$data) is not reliable.
    $audience_type = $data->getAudienceType();
    $global = $audience_type['context'] == 'global';

    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'union' && in_array($data_preset_name, $preset->config)) {
        if ($global) {
          audience_preset_update($preset);
        }
        else {
          $context = $data->getContext();
          $audience = new Audience($data_preset_name, $context);
          audience_update($audience);
        }
      }
    }
  }
}