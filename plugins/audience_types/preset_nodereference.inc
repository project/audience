<?php

/**
 * @file
 *  provides the 'preset_nodereference' audience type to audience.
 *
 *  The audience consists of users defined in the selected userreference field
 *  of the given node.
 */

if (module_exists('nodereference')) {
  $plugin = array(
    'label' => t('Audience Preset by NodeReference Field'),
    'context' => 'node',
    'description' => t('Declare users by applying an audience preset to the referenced nodes.'),
    'members callback' => '_audience_type_members_preset_nodereference',
    'is member callback' => '_audience_type_is_member_of_preset_nodereference',
    'circumstances changed callback' => '_audience_type_preset_nodereference_circumstances_changed',
    'selector options' => 'audience_presets_as_optionsarray',
    'selector title' => t('Audience Preset'),
    'selector description' => t('Select an audience preset that should be applied to the referenced nodes.'),
    'config options' => '_audience_type_config_options_preset_nodereference',
    'config title' => t('NodeReference Field'),
    'config description' => t('Select the nodereference field to fetch the node context for the selected audience preset.'),
    'config multiple' => TRUE,
  );
}

/**
 * Callback function to check if user is listed in the selected audience of the
 * given nodereference nodes.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_preset_nodereference($audience_preset, $context, $account) {
  if ($node = _audience_get_node_object_from_context($context)) {

    $referenced_preset = $audience_preset->selector;
    $field_names = $audience_preset->config;

    foreach ($field_names as $field_name) {
      if (isset($node->$field_name) && is_array($node->$field_name)) {
        foreach ($node->$field_name as $item) {
          $nid = $item['nid'];
          $referenced_context = array('node' => node_load($nid, NULL, _audience_node_comes_from_nodeapi($nid)));

          if (audience_is_member_of($referenced_preset, $referenced_context, $account)) {
            return TRUE;
          }
        }
      }
    }
  }
  return FALSE;
}

/**
 * Callback to get the user ids for the given traversed preset.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_preset_nodereference($audience_preset, $context = array()) {
  $members = array();
  if ($node = _audience_get_node_object_from_context($context)) {

    $referenced_preset = $audience_preset->selector;
    $field_names = $audience_preset->config;

    foreach ($field_names as $field_name) {
      if (isset($node->$field_name) && is_array($node->$field_name)) {
        foreach ($node->$field_name as $item) {
          if (!empty($item['nid'])) {
            $nid = $item['nid'];
            $referenced_context = array('node' => node_load($nid, NULL, _audience_node_comes_from_nodeapi($nid)));
            $add = audience_members($referenced_preset, $referenced_context);
            $members = array_merge($members, $add);
          }
        }
      }
    }
  }
  return array_unique($members);
}

/**
 * Helper function to retrieve the nodereference fields.
 */
function _audience_type_config_options_preset_nodereference() {
  $fields = content_fields();
  $return = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'nodereference') {
      $text = t('@field_name: @field_label', array('@field_label' => $field['widget']['label'], '@field_name' => $name));
      $text = (drupal_strlen($text) > 64) ? drupal_substr($text, 0, 64) .'...' : $text;
      $return[$name] = $text;
    }
  }
  ksort($return);
  return $return;
}

/**
 * Callback function to react on changed circumstances for the preset_nodereference audience type.
 *
 * @param string $mode
 * @param mixed $data
 */
function _audience_type_preset_nodereference_circumstances_changed($mode, $data) {
  // An audience preset has updated, so we need to update presets, that contain
  // that preset.
  if (drupal_substr($mode, 0, 16) == 'audience_preset:') {
    $data_preset_name = $data->name;
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'preset_nodereference' && $data_preset_name == $preset->selector) {
        audience_preset_update($preset);
      }
    }
  }
  // If any node was updated, deleted or whatever, and so its audience changed
  // it might be referenced by a preset_nodereference audience.
  elseif (drupal_substr($mode, 0, 4) == 'node') {
    $node = $data;

    // As looking for the bunch of nodes that might be refernce to this one is
    // needs to build some queries, we simply force an update for each preset_nodereference
    // audience.
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'preset_nodereference') {
        audience_preset_update($preset);
      }
    }
  }
}
