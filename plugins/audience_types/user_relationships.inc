<?php


/**
 * @file
 *  provides the 'user_relationships' audience type to audience.
 *
 *  The audience consists of users that are connected via a user_relationship
 *  to the author of the given node.
 */

if (module_exists('user_relationships_api')) {
  $plugin = array(
    'label' => t('User Relationship'),
    'description' => t('Declare related users as audience.'),
    'context' => 'node',
    'members callback' => '_audience_type_members_user_relationship',
    'is member callback' => '_audience_type_is_member_of_user_relationship',
    'selector options' => '_audience_type_selector_options_user_relationship',
    'selector title' => t('User relationship type'),
    'selector description' => t('Select the user relationship type that shall define the audience for the then-given node author.'),
  );
}

/**
 * Callback function to check if user is listed in the selected userreference
 * field of the given node.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_user_relationship($audience_preset, $context, $account) {
  if ($node = _audience_get_node_object_from_context($context)) {

    $rtid = $audience_preset->selector;
    $params = array(
      'between' => array($node->uid, $account->uid),
      'approved' => TRUE,
    );
    // If rtid > 0 a relationship type was selected
    if ($rtid !== "") {
      $params['rtid'] = array($rtid);
    }
    return (boolean) user_relationships_load($params, array('count' => TRUE));
  }
  return FALSE;
}

/**
 * Callback to get the user ids for the given userreference field in the given node.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_user_relationship($audience_preset, $context = array()) {
  if ($node = _audience_get_node_object_from_context($context)) {
    $rtid = $audience_preset->selector;
    $params = array(
      'user' => $node->uid,
      'approved' => TRUE,
    );
    // If rtid > 0 a relationship type was selected
    if ($rtid !== '') {
      $params['rtid'] = array($rtid);
    }

    $relationships = user_relationships_load($params);
    $users = array();
    // Search in two way and one way relations.
    foreach ($relationships as $relationship) {
      if ($relationship->requester_id != $node->uid) {
        $users[$relationship->requester_id] = $relationship->requester_id;
      }
      if ($relationship->requestee_id != $node->uid) {
        $users[$relationship->requestee_id] = $relationship->requestee_id;
      }
    }
    return $users;
  }
  return array();
}

/**
 * Helper function to retrieve the userrefernce fields.
 */
function _audience_type_selector_options_user_relationship() {
  $relationship_types = array(
    '' => t('- Any relationship -'),
  );
  foreach (user_relationships_types_load() as $rtid => $relationship) {
    $relationship_types[$rtid] = $relationship->name;
  }
  return $relationship_types;
}
