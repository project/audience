<?php


/**
 * @file
 *   provides the 'user' audience type to audience.
 */

$plugin = array(
  'label' => t('User'),
  'description' => t('A single user can be declared as audience.'),
  'context' => 'global',
  'members callback' => '_audience_type_members_user',
  'is member callback' => '_audience_type_is_member_of_user',
  'circumstances changed callback' => '_audience_type_user_circumstances_changed',
  'selector title' => t('User'),
  'selector description' => t('Select a user to be the "audience".'),
  'selector options' => '_audience_type_selector_options_user',
);

/**
 * Callback function to specify if user is the user defined in preset.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_user($audience_preset, $context, $account) {
  $audience_preset_user = $audience_preset->selector;
  return ($account->uid == $audience_preset_user);
}

/**
 * Get the uid array for the given user. (So only the uid of the given user).
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_user($audience_preset, $context) {
  $audience_preset_user = $audience_preset->selector;
  return array($audience_preset_user);
}

/**
 * Callback function to retrieve all current users.
 */
function _audience_type_selector_options_user() {
  $users = &ctools_static(__FUNCTION__);

  if (empty($users)) {
    $users = array();
    $res = db_query("SELECT uid, name FROM {users} WHERE uid > 0 AND name<>'' ORDER BY name ASC");
    while ($obj = db_fetch_object($res)) {
      $users[$obj->uid] = $obj->name;
    }
  }
  return $users;
}


/**
 * Callback function to react on changed circumstances for the user audience type.
 *
 * @param string $mode
 *   only relevant for the moment: user:delete
 * @param mixed $data
 *   this should be a user object for user:delete
 *
 */
function _audience_type_user_circumstances_changed($mode, $data) {
  if ($mode == 'user:delete') {
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'user' && $preset->selector == $data->uid) {
        // We will not delete the preset, at is could be reconfigured, but will
        // show that the audience is deleted.
        $audience = new Audience($preset->name, array());
        audience_delete($audience);
      }
    }
  }
}
