<?php


/**
 * @file
 *   provides the 'role' audience type to audience.
 */

$plugin = array(
  'label' => t('Role'),
  'description' => t('Declare a user role as audience.'),
  'context' => 'global',
  'members callback' => '_audience_type_members_role',
  'is member callback' => '_audience_type_is_member_of_role',
  'circumstances changed callback' => '_audience_type_roles_circumstances_changed',
  'selector title' => t('User role'),
  'selector description' => t('Select a user role for to be the "audience".'),
  'selector options' => 'user_roles',
);

/**
 * Callback function to specify if user is in role selected by preset.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_role($audience_preset, $context, $account) {
  $audience_preset_role = $audience_preset->selector;
  return key_exists($audience_preset_role, $account->roles);
}

/**
 * Retrieve all users of a given role.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_role($audience_preset, $context = array()) {
  $users_roles = &ctools_static(__FUNCTION__);
  $role_id = $audience_preset->selector;

  if (!isset($users_roles[$role_id])) {
    switch ($role_id) {
      case DRUPAL_ANONYMOUS_RID:
        $users[$role_id] = array(0);
        break;
      case DRUPAL_AUTHENTICATED_RID:
        $res = db_query('SELECT uid FROM {users} WHERE uid != 0');
        $users = array();
        while ($u = db_fetch_object($res)) {
          $users[$u->uid] = $u->uid;
        }
        $users_roles[$role_id] = $users;
        break;
      default:
        $res = db_query('SELECT uid FROM {users_roles} WHERE rid = %d', $role_id);
        $users = array();
        while ($u = db_fetch_object($res)) {
          $users[$u->uid] = $u->uid;
        }
        $users_roles[$role_id] = $users;
        break;
    }
  }
  return $users_roles[$role_id];
}

/**
 * Callback function to react on changed circumstances for the roles audience type.
 *
 * @param string $mode
 *   only relevant for the moment: role:...
 * @param mixed $data
 *   this should be an array with role information (like 'rid' => ..)
 *
 */
function _audience_type_roles_circumstances_changed($mode, $data) {
  if (drupal_substr($mode, 0, 4) == 'role') {
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'roles' && $preset->selector == $data['rid']) {
        // Each audience preset containing role rid has to be updated.
        audience_preset_update($preset);
      }
    }
  }
}
