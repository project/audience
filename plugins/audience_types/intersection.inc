<?php

/**
 * @file
 *   provides an audience type to intersect presets.
 */

$plugin = array(
  'label' => t('Intersection of presets'),
  'description' => t('Build a preset by intersect several presets.'),
  'context' => 'node',
  'members callback' => '_audience_type_members_intersection',
  'is member callback' => '_audience_type_is_member_of_intersection',
  'circumstances changed callback' => '_audience_type_intersection_circumstances_changed',
  'config title' => t('Presets'),
  'config description' => t('Select the presets to <em>AND</em>.'),
  'config options' => 'audience_presets_as_optionsarray',
  'config multiple' => TRUE,
);

/**
 * Callback function to specify if user is in ALL presets defined by the given
 * intersection.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_intersection($audience_preset, $context = array(), $account) {
  $presets = $audience_preset->config;

  // Are any presets selected?
  if (is_array($presets) && count($presets)) {
    foreach ($presets as $preset_name) {
      $return = audience_is_member_of($preset_name, $context, $account);
      // Return false on failure, as user has to be member of ALL presets.
      if (!$return) {
        return FALSE;
      }
    }
    // If no one returned false, user is member of all presets defined.
    if ($return) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Callback function to retrieve the intersection of users in the given presets.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_intersection($audience_preset, $context = array()) {
  $presets = $audience_preset->config;

  // Are any presets selected?
  if (is_array($presets) && count($presets)) {
    $users = NULL;
    foreach ($presets as $preset_name) {
      $add_users = audience_members($preset_name, $context);

      // If current preset has no users, intersection does too.
      if (!count($add_users)) {
        return array();
      }
      // In first run, $users is not set
      elseif (!isset($users)) {
        $users = $add_users;
      }
      else {
        $users = array_intersect($users, $add_users);
      }

      // Return empty for empty intersection.
      if (!count($users)) {
        return array();
      }
    }
    return $users;
  }
  return array();
}

/**
 * Callback function to react on changed circumstances for the intersection audience type.
 *
 * @param string $mode
 * @param mixed $data
 */
function _audience_type_intersection_circumstances_changed($mode, $data) {
  // An audience preset has updated, so we need to update presets, that contain
  // that preset.
  if (drupal_substr($mode, 0, 16) == 'audience_preset:') {
    $data_preset_name = $data->name;
    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'intersection' && in_array($data_preset_name, $preset->config)) {
        audience_preset_update($preset);
      }
    }
  }
  // An audience is affected, so we have to update audiences, that contain the
  // given audience as intersection element.
  elseif (drupal_substr($mode, 0, 8) == 'audience') {
    $data_preset_name = $data->getPresetName();
    // Check if the audience is global. Checking on context(=$data) is not reliable.
    $audience_type = $data->getAudienceType();
    $global = $audience_type['context'] == 'global';

    $presets = audience_get_presets();
    foreach ($presets as $preset) {
      if ($preset->audience_type == 'intersection' && in_array($data_preset_name, $preset->config)) {
        if ($global) {
          audience_preset_update($preset);
        }
        else {
          $context = $data->getContext();
          $audience = new Audience($data_preset_name, $context);
          audience_update($audience);
        }
      }
    }
  }
}