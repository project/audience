<?php

/**
 * @file
 *  provides the 'author' audience type to audience.
 *
 *  The audience consists of the author of the given node, and/or the node
 *  editors (as stored in the node revisions table).
 */

$plugin = array(
  'label' => t('Author'),
  'description' => t('Declare node author and/or editors as audience.'),
  'context' => 'node',
  'members callback' => '_audience_type_members_author',
  'is member callback' => '_audience_type_is_member_of_author',
  // 'circumstances changed callback' => '...', Does not need one, as update is recognized by context update.
  'selector options' => array(
    'author' => t('Current author only'),
    'editor' => t('Editors only'),
    'author or editor' => t('Author or editor'),
    'author and editor' => t('Author and editor'),
  ),
  'selector title' => t('Author / Editor'),
  'selector description' => t('Either author (the owner of the current node) and/or editors (node revision users) can be declared as audience preset.'),
);

/**
 * Callback function to specify if user is author or editor of the given context node.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @param $account
 *   the user object (at least as it is located in global $user) of the user to
 *   check against
 * @return bool
 */
function _audience_type_is_member_of_author($audience_preset, $context, $account) {
  if ($node = _audience_get_node_object_from_context($context)) {

    $selector = $audience_preset->selector;
    // Check on author
    if (($selector == 'author' || $selector == 'author or editor') && $node->uid == $account->uid) {
      return TRUE;
    }
    // Search revision information for editor selectors.
    elseif (strpos($selector, 'editor') !== FALSE) {
      $is_editor = db_result(db_query('SELECT uid FROM {node_revisions} WHERE nid = %d AND uid = %d', $node->nid, $account->uid));

      if ($selector == 'author and editor') {
        return ($is_editor && ($node->uid == $account->uid));
      }
      else {
        return $is_editor;
      }
    }
  }
  return FALSE;
}

/**
 * Callback function to retrieve author and/or editors of the given context node.
 *
 * @param $audience_preset
 *   the preset definition for the audience to check on.
 * @param $context
 *   an array of context objects, in a lot of cases this might be a node, given
 *   as array('node' => $node)
 * @return
 *   array of user ids that are member of the audience
 */
function _audience_type_members_author($audience_preset, $context = array()) {
  if ($node = _audience_get_node_object_from_context($context)) {

    $selector = $audience_preset->selector;
    // Author has to be ediotor
    if ($selector == 'author and editor') {
      $is_editor = db_result(db_query('SELECT uid FROM {node_revisions} WHERE nid = %d AND uid = %d', $node->nid, $node->uid));
      return ($is_editor) ? array($node->uid) : array();
    }
    // Search revision information for editor selectors.
    elseif (strpos($selector, 'editor') !== FALSE) {
      $res = db_query('SELECT uid FROM {node_revisions} WHERE nid = %d', $node->nid);
      $uids = array();
      // Push results in array()
      while ($user = db_fetch_object($res)) {
        $uids[$user->uid] = $user->uid;
      }

      // Add author
      if ($selector == 'author or editor') {
        $uids[$node->uid] = $node->uid;
      }
      return $uids;
    }

    // Check on author
    if ($selector == 'author') {
      return array($node->uid);
    }
  }
  // return an empty array for any other.
  return array();
}