
@author: derhasi

Audience provides an abstract definition of user groups and types. These
definitions are called "Audience presets" and can be used by other modules
(like Field Access Field) to control different types of audience related actions
or permissions.
By using CTools these audience presets are exportables and can either get stored
in a database or live in code provided by modules.

An "Audience" is derived from applying an audience preset to a given context (
currently only 'global' and 'node' context are supported). So we can get an
audience "editors from node 15" by using a preset (of audience type 'author')
and applying it to node 15.

Modules can use these presets to check if a user is member of a given audience.
Therefore <?php audience_is_member_of($preset, $context, $account) ?> is used.
Using <?php audience_members($preset, $context) ?> all users of a given audience
in the given context can be retrieved.

Audience presets are instances defined by an audience type. Default audience
types shipped with Audience are "user", "roles", "author" and "userreference".
Audience Bundle also declares audience types "audience_bundle_or" and
"audience_bundle_and".

By default an audience is not stored anywhere, but modules like audience_acl can
be used to do so, e.g. cache it for node_access integration.

Sponsors
========
Development is sponsored by Zehnplus (zehnplus.ch) and undpaul (undpaul.de)
