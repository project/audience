<?php

/**
 * @file
 *   Provides a basic class to represent a single audience.
 */


class Audience {

  /**
   * The audience preset as object for this audience.
   *
   * @var object
   */
  protected $preset;

  /**
   * The audience preset machine name.
   *
   * @var string
   */
  protected $preset_name;

  /**
   * The audience type as object for this audience.
   *
   * @var object
   */
  protected $audience_type;

  /**
   * The audience type machine name.
   *
   * @var string
   */
  protected $audience_type_name;

  /**
   * Context as array.
   *
   * @var array
   */
  protected $context;

  /**
   * Context ID, relevant for static cache and co.
   *
   * @var string
   */
  protected $context_id;

  /**
   * Constructor for the Audience class. This allways needs a given preset and
   * a context (an empty array means global context).
   *
   * @param string $preset_name
   * @param array $context
   *
   */
  function __construct($preset_name, $context = array()) {
    $this->preset_name = $preset_name;
    $this->context = $context;
  }

  /**
   * Get the preset object when needed.
   */
  function getPreset() {
    // Assure ctools export is loaded, as preset could be present as a stored value.
    ctools_include('export');
    if (!isset($this->preset)) {
      $this->preset = audience_get_preset($this->preset_name);
    }
    return $this->preset;
  }
  /**
   * The initial preset name.
   */
  function getPresetName() {
    return $this->preset_name;
  }

  /**
   * Get the context id when needed.
   */
  function getContextID() {
    if (!isset($this->context_id)) {
      $this->context_id = _audience_context_id($this->getPresetName(), $this->context);
    }
    return $this->context_id;
  }

  /**
   * The initial context.
   */
  function getContext() {
    return $this->context;
  }

  /**
   * Get the audience type object when needed.
   */
  function getAudienceType() {
    if (!isset($this->audience_type)) {
      $name = $this->getAudienceTypeName();
      $this->audience_type = audience_get_type($name);
    }
    return $this->audience_type;
  }

  /**
   * Get the audience type name/id when needed.
   */
  function getAudienceTypeName() {
    if (!isset($this->audience_type_name)) {
      $this->audience_type_name = $this->getPreset()->audience_type;
    }
    return $this->audience_type_name;
  }

  /**
   * Retrieve the members of this audience.
   */
  function getMembers() {
    $preset = $this->getPreset();
    // Get callback for the presets audience type.
    $members_callback = ctools_plugin_load_function('audience', 'audience_types', $preset->audience_type, 'members callback');
    // Check on callback, if it exists.
    if ($members_callback) {
      return $members_callback($preset, $this->getContext());
    }
    else {
      // Else return FALSE, as no callback could be found.
      return FALSE;
    }
  }

  /**
   * Check if the given user is member of the audience.
   *
   * @param $account
   *   a user object
   * @return bool
   */
  function isMember($account) {
    $preset = $this->getPreset();
    // Get callback for the preset audience type.
    $is_member_of_callback = ctools_plugin_load_function('audience', 'audience_types', $preset->audience_type, 'is member callback');
    // Check on callback, if it exists.
    if ($is_member_of_callback) {
      return $is_member_of_callback($preset, $context, $account);
    }
    // Else return FALSE, as no callback could be found.
    return FALSE;
  }

}