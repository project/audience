<?php

/**
 * @file
 *   Admin forms and theme functions.
 */

/**
 * Theme function to provide a draggable preset selection in audience_nodeaccess field settings.
 */
function theme_audience_nodeaccess_audience_presets_selection($element) {

  $body = "";

  $children = element_children($element);

  $weight_header = "";
  drupal_add_tabledrag('audience_nodeaccess-audience-presets-selection-table', 'order', 'sibling', 'audience_nodeaccess-audience-preset-weight');

  $rows = array();
  foreach ($children as $key) {
    $element[$key]['weight']['#attributes']['class'] .= "audience_nodeaccess-audience-preset-weight audience_nodeaccess-audience-preset-weight-$key";
    if (!$weight_header) {
      $element[$key]['weight']['#title'];
    }
    unset($element[$key]['weight']['#title']);

    $weight = drupal_render($element[$key]['weight']);
    $rest = drupal_render($element[$key]);
    $rows[] = array(
      'data' => array($rest, $weight),
      'class' => 'draggable',
    );
  }

  $header = array(
    t('Audience preset'),
    $weight_header,
  );

  $body = theme('table', $header, $rows, array('id' => 'audience_nodeaccess-audience-presets-selection-table'));

  return '<fieldset' . drupal_attributes($element['#attributes']) . '>' . ($element['#title'] ? '<legend>' . $element['#title'] . '</legend>' : '') . (isset($element['#description']) && $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '') . $body . (!empty($element['#children']) ? $element['#children'] : '') . (isset($element['#value']) ? $element['#value'] : '') . "</fieldset>\n";
}
