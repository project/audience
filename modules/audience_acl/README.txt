
This module provides basic functionality to store audience related ACLs. Therefore
it uses the ACL module [http://drupal.org/project/acl].

Attention!: Issue http://drupal.org/node/695852 has to be fixed for that.

