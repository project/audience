<?php

/**
 * @file
 *  This file provieds functions to make better use out of acl.module, that
 *  is mainly used to cache audience members for given context and make
 *  them appliable for nodeaccess module.
 */


/**
 * Implementation of hook_nodeapi().
 *
 * Collects data passed by other modules using audience_acl to set node records
 * via acl.
 */
function audience_acl_nodeapi(&$node, $op) {
  switch ($op) {
    case 'insert':
    case 'update':
      // Clear in any case, to assure old values are deleted.
      acl_node_clear_acls($node->nid, 'audience');

      // There were some values set for audience acl.
      if (isset($node->audience_acl) && is_array($node->audience_acl) && !empty($node->audience_acl)) {

        $aclids = array();
        // Run through all values set, and OR the different specifications for
        // the same acl id.
        foreach ($node->audience_acl as $spec) {
          $acl_id = $spec['acl_id'];
          if ($acl_id) {
            // Set default for this acl_id
            if (!isset($aclids[$acl_id])) {
              $aclids[$acl_id] = array('view' => FALSE, 'update' => FALSE, 'delete' => FALSE);
            }
            // Specification with higher priority overwrites other specs.
            if (isset($spec['priority']) && (!isset($aclids[$acl_id]['priority']) || $aclids[$acl_id]['priority'] < $spec['priority'])) {
              $aclids[$acl_id] = $spec;
            }
            // Merge specifications on same priority
            elseif ($aclids[$acl_id]['priority'] == $spec['priority']) {
              $keys = array('view', 'update', 'delete');
              foreach ($keys as $key) {
                $aclids[$acl_id][$key] = ($aclids[$acl_id][$key] || $spec[$key]);
              }
              $aclids[$acl_id]['priority'] = $spec['priority'];
            }
            // else priority is not hight enough.
          }
        }

        // Write the collected audience acl settings.
        foreach ($aclids as $aclid => $grants) {
          acl_node_add_acl($node->nid, $aclid, $grants['view'], $grants['update'], $grants['delete'], $grants['priority'], TRUE);
        }
      }
      break;
  }

}

//============ ACL ==============//

/**
 * Implementation of hook_enabled().
 *
 * To implement audience acl values in acl.module.
 */
function audience_enabled() {
  return TRUE;
}

/**
 * Wrapper function to create or update an ACL via given preset.
 *
 * @param string $preset_name
 *  string identifier of the audience preset
 * @param array $context
 *  currently only array('node' => $nid) is supported to pass a node as context
 * @return integer
 *  a single ACL ID.
 */
function audience_set_acl($preset_name, $context = array()) {
  $preset = audience_get_preset($preset_name);
  $aclid = _audience_set_acl($preset_name, $context);
  return $aclid;
}


/**
 * Wrapper function to get ACL idfor a given preset/context.
 */
function audience_get_acl($preset_name, $context = array(), $reset = FALSE) {
  $static = &ctools_static(__FUNCTION__, array(), $reset);
  $context_id = _audience_context_id($preset_name, $context);

  // There is allready a static cached version.
  if (!isset($static[$preset_name][$context_id])) {
    $static[$preset_name][$context_id] = audience_set_acl($preset_name, $context);
  }
  return $static[$preset_name][$context_id];
}

/**
 * Retrieves all audiences for a given user, by running through the acl cache.
 */
function audience_alc_get_by_user($uid, $reset = FALSE) {
  if (!$uid) {
    return array();
  }
  $static = &ctools_static(__FUNCTION__ .'--'. $uid, FALSE, $reset);

  if ($static === FALSE) {
    $query = 'SELECT a.* FROM {acl} a LEFT JOIN {acl_user} u ON (a.acl_id = u.acl_id) WHERE u.uid = %d';
    $res = db_query($query, $uid);
    while ($obj = db_fetch_object($res)) {
      $static[$obj->acl_id] = array(
        'preset' => $obj->name,
        'context' => $obj->number,
      );
    }
  }
  return $static;
}


/**
 * Helper function for ACL to set the acl for the given preset context
 * combination, and return the aclid for that.
 *
 * @param string $preset_name
 *  string identifier of the audience preset
 * @param array $context
 *  currently only array('node' => $nid) is supported to pass a node as context
 * @return integer
 *  a single ACL ID.
 *  or FALSE for invalid preset/context combination (see context in audience_type)
 */
function _audience_set_acl($preset_name, $context = array()) {
  $members = audience_members($preset_name, $context);

  $context_id = _audience_context_id($preset_name, $context);

  // Only store and update valid
  if ($context_id !== FALSE) {
    $aclid = db_result(db_query("SELECT acl_id FROM {acl} WHERE module = '%s' AND name ='%s' AND number = %d", 'audience', $preset_name, $context_id));
    if (!$aclid) {
      $aclid = acl_create_new_acl('audience', $preset_name, $context_id);
    }
    _audience_acl_add_users($aclid, $members);
    return $aclid;
  }
  return FALSE;
}

/**
 * Helper function to remove existing and add multiple users to the acl_users table.
 */
function _audience_acl_add_users($aclid, $uids) {
  acl_remove_all_users($aclid);

  // Only set new ones, if there are some.
  if (count($uids)) {
    $query = 'INSERT INTO {acl_user} (acl_id, uid) VALUES ';
    $args = array();
    $comma = FALSE;
    foreach ($uids as $uid) {
      if ($comma) {
        $query .= ', ';
      }
      $query .= '(%d, %d)';
      $args[] = $aclid;
      $args[] = $uid;
      $comma = TRUE;
    }
    db_query($query, $args);
  }
}


/**
 * Implementation of hook_audience_update().
 *
 * @param Audience $audience
 */
function audience_acl_audience_update($audience) {
  // The audience has been updated, so we have to recalculate the cache.
  _audience_set_acl($audience->getPresetName(), $audience->getContext());
}

/**
 * Implementation of hook_audience_preset_update().
 *
 * @param object $preset
 */
function audience_acl_audience_preset_update($preset) {

  // Get all defined ACL id with the given preset. - we only need the number for
  // further processing, as it builds the node.
  $result = db_query("SELECT number FROM {acl} WHERE module = '%s' AND name = '%s'", 'audience', $preset->name);
  while ($obj = db_fetch_object($result)) {
    // As we can only deal with nodes as context, we do so.
    $context = ($obj->number) ? array('node' => node_load($obj->number)) : array();
    _audience_set_acl($preset->name, $context);
  }
}