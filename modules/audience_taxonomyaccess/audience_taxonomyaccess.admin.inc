<?php

/**
 * @file
 *  Configuration forms for audience taxonomyaccess.
 */

/**
 * Basic Settings form for audience taxonomyaccess.
 */
function audience_taxonomyaccess_basic_settings(&$form, &$form_state) {
  $form = array();
  $form['audience_taxonomyaccess_node_types'] = array(
    '#type' => 'select',
    '#title' => t('Node types'),
    '#description' => t('Select the node types that can be used for audience associtation in term access settings.'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('audience_taxonomyaccess_node_types', array()),
    '#multiple' => TRUE,
  );
  return system_settings_form($form);
}


/**
 * Form for implementing audience taxonomyaccess settings
 */
function _audience_taxonomyaccess_term_form(&$form, &$form_state) {
  if (user_access('administer audience acl terms')) {
    $tid = (isset($form['tid']['#value']) ? $form['tid']['#value'] : NULL);

    $config = _audience_taxonomyaccess_setting_load($tid);
    // Add an empty one for multiple values
    $config['new'] = array('audience' => '', 'node' => '', 'grants' => array());

    $form['audience_taxonomyaccess'] = array(
      '#type' => 'fieldset',
      '#title' => t('Access control via Audiences'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );

    $nodes = audience_taxonomyaccess_nodes();
    $nodes[''] = t('<None>');
    $presets = array_merge(array('' => ''), audience_presets_as_optionsarray());

    // Run each config
    foreach ($config as $key => $spec) {
      if ($key == 'new' || (isset($spec['preset'], $spec['node']) && $spec['preset'])) {
        $name = ($key === 'new') ? t('Add a new audience') : t('@preset - @node (@grants)', array('@preset' => $spec['preset'], '@node' => $nodes[$spec['node']], '@grants' => implode(', ', $spec['grants'])));
        $form['audience_taxonomyaccess'][$key] = array(
          '#type' => 'fieldset',
          '#title' => $name,
          '#collapsible' => TRUE,
          '#collapsed' => ($key != 'new'),
          '#tree' => TRUE,
        );
        $form['audience_taxonomyaccess'][$key]['preset'] = array(
          '#type' => 'select',
          '#title' => t('Audience Preset'),
          '#options' => $presets,
          '#default_value' => $spec['preset'],
        );
        $form['audience_taxonomyaccess'][$key]['node'] = array(
          '#type' => 'select',
          '#title' => t('Node'),
          '#options' => $nodes,
          '#default_value' => $spec['node'],
        );
        $form['audience_taxonomyaccess'][$key]['grants'] = array(
          '#type' => 'checkboxes',
          '#options' => array('view' => t('View'), 'update' => t('Edit'), 'delete' => t('Delete')),
          '#default_value' => $spec['grants'],
        );
      }

    }
    $form['audience_taxonomyaccess']['audience_taxonomyaccess_add'] = array(
      '#type' => 'submit',
      '#value' => t('Store and Edit'),
      '#tree' => FALSE,
    );
    $form['#submit'][] = 'audience_taxonomyaccess_form_submit';
  }
}

/**
 * Submission function to store the node forum audience setting.
 */
function audience_taxonomyaccess_form_submit($form, &$form_state) {
  $tid = $form_state['values']['tid'];
  $values = $form_state['values']['audience_taxonomyaccess'];
  _audience_taxonomyaccess_setting_save($tid, $values);

  if ($form_state['values']['op'] == t('Store and Edit')) {
    unset($form_state['redirect']);
  }
}
